package com.example.laboratorio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_resultado.*

class activity_resultado : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)

        val origen:Bundle?=intent.extras
        origen?.let {
            val nombre:String=origen.getString("kNombre","")?:""
            val edad:Int=origen.getInt("kEdad",0)?:0
            var especie:String=origen.getString("kEspecie","")?:""
            val vacuna1:Boolean=origen.getBoolean("kVacuna1",false)?:false
            val vacuna2:Boolean=origen.getBoolean("kVacuna2",false)?:false
            val vacuna3:Boolean=origen.getBoolean("kVacuna3",false)?:false

            if (especie==getString(R.string.especie1)){
                imgEspecie.setImageResource(R.drawable.icon_perro)
            }else if(especie==getString(R.string.especie2)){
                imgEspecie.setImageResource(R.drawable.icon_gato)
            }else if(especie==getString(R.string.especie3)){
                imgEspecie.setImageResource(R.drawable.icon_conejo)
            }


            var vacuna:String="Vacunas aplicadas:"
            if (vacuna1){
                vacuna = vacuna + "\n  -" + getString(R.string.vacuna1)
            }
            if(vacuna2){
                vacuna = vacuna + "\n  -" + getString((R.string.vacuna2))
            }
            if(vacuna3 ){
                vacuna = vacuna + "\n  -" +  getString(R.string.vacuna3)
            }

            txt2Nombre.text=nombre
            txt2Edad.text=edad.toString()
            txt2Especie.text=especie
            txt2Vacuna.text=vacuna
        }
    }
}