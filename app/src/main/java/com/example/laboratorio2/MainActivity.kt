package com.example.laboratorio2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegistar.setOnClickListener(){
            if (txtNombre.text.isNullOrEmpty()){
                Toast.makeText(this, "Debe ingresar el nombre de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else if(txtEdad.text.isNullOrEmpty()){
                Toast.makeText(this, "Debe ingresar la edad de su mascota", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val nombre:String=txtNombre.text.toString()
            val edad:Int=txtEdad.text.toString().toInt()
            var especie:String=""
            val vacuna1 :Boolean= chkVacuna1.isChecked
            val vacuna3:Boolean=chkVacuna2.isChecked
            val vacuna2:Boolean=chkVacuna3.isChecked

            if (rdbEspecie1.isChecked){
                especie=getString(R.string.especie1)
            }else if(rdbEspecie2.isChecked){
                especie=getString(R.string.especie2)
            }else if (rdbEspecie3.isChecked){
                especie=getString(R.string.especie3)
            }

            var info:Bundle=Bundle().apply {
                this.putString("kNombre",nombre)
                this.putInt("kEdad",edad)
                this.putString("kEspecie",especie)
                this.putBoolean("kVacuna1",vacuna1)
                this.putBoolean("kVacuna2",vacuna2)
                this.putBoolean("kVacuna3",vacuna3)
            }
            val intent:Intent=Intent(this,activity_resultado::class.java)
            intent.putExtras(info)
            startActivity(intent)
        }
    }
}